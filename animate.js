
const navbar = document.getElementById("mob");
const hamburger = document.getElementById('ham');
const topp = document.getElementById('top');

hamburger.addEventListener('click',(ele)=>{
    if(navbar.style.display=="none"){
        navbar.style.display="block";
        hamburger.style.backgroundImage='url("/images/icon-close.svg")';
        topp.style.backgroundColor="rgba(0,0,0,0.5)";
    }else{
        navbar.style.display="none";
        hamburger.style.backgroundImage='url("/images/icon-hamburger.svg")';
        topp.style.backgroundColor="hsl(0, 0%, 98%)";
    }
    
});